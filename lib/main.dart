// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:calculator/UI/Screens/secondPage.dart';
import 'package:flutter/material.dart';

import 'UI/Screens/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      themeMode: ThemeMode.system,


      home: OrientationBuilder(
        builder: (context,orientation) {
          if(orientation == Orientation.portrait)
          return Standard();

          return Scintefic();
        }
      ),

    );
  }
}
