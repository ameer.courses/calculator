// ignore_for_file: prefer_final_fields

import 'package:get/get.dart';

class CalcController extends GetxController{
  String _first = '';
  String _second = '';

  String get first => _first;

  String get second => _second;

  void press(String value){
   _first = _second;
   _second = value;
   update();
  }

}