import 'package:flutter/material.dart';


class Screen2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Screen 2'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Back to home page'),
          onPressed: (){
            //if(Navigator.canPop(context))
            Navigator.maybePop(context);
            //Navigator.pushReplacementNamed(context, '/home');

          },
        ),
      ),
    );
  }
}
