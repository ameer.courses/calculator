// ignore_for_file: prefer_const_constructors

import 'package:calculator/UI/Elements/Bottons.dart';
import 'package:calculator/UI/Elements/output.dart';
import 'package:flutter/material.dart';

class Standard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    List<List<String>> buttons = [
      ['DEL','='],
      ['7','8','9','+'],
      ['4','5','6','-'],
      ['1','2','3','×'],
      ['.','0','C','÷'],
    ];

    return Scaffold(

        // body: Center(
        //         child: ElevatedButton(
        //           child: Text('go to Screen 2'),
        //           onPressed: (){
        //             Navigator.pushReplacementNamed(context, '/Screen2');
        //           },
        //         ),
        // )
        body: SafeArea(
         child: Column(
           children: [
             Expanded(
                 flex: 2,
                 child: OutputWidget()
             ),
             for(var row in buttons )
               Expanded(
                   child: Row(
                     children: [
                       for(var b in row)
                         CalcButton(b),
                     ],
                   )
               ),


           ],
         )
      )
    );
  }
}

class Scintefic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        // body: Center(
        //         child: ElevatedButton(
        //           child: Text('go to Screen 2'),
        //           onPressed: (){
        //             Navigator.pushReplacementNamed(context, '/Screen2');
        //           },
        //         ),
        // )
        backgroundColor: Colors.blue,
        body: SafeArea(
          child: Center(
            child: Text('Scintefic'),
          ),
        ));
  }
}
