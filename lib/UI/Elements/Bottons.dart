import 'package:calculator/Controllers/calcController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CalcButton extends StatelessWidget {

  final String text;
  static final CalcController controller = Get.find();

  CalcButton(this.text);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Expanded(
        child: GestureDetector(
          onTap: (){
            controller.press(text);
          },
          child: Container(
            color: Colors.blueGrey,
            width: double.infinity,
            height: double.infinity,
            margin: EdgeInsets.all(size.width*0.025),
            padding: EdgeInsets.all(size.width*0.015),
            child: FittedBox(
                child: Text(text),
            ),
          ),
        )
    );
  }

}
