// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:calculator/Controllers/calcController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OutputWidget extends StatelessWidget {

  static final controller = Get.put(CalcController());

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GetBuilder<CalcController>(
      init: controller,
      builder: (controller) {
        return Column(
          children: [
            Expanded(
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  margin: EdgeInsets.all(size.width*0.025),
                  child: FittedBox(
                      child: Text(controller.first),
                      alignment: Alignment.centerLeft,
                  ),
                )
            ),
            Expanded(
              flex: 2,
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  margin: EdgeInsets.all(size.width*0.025),
                  child: FittedBox(
                      child: Text(controller.second),
                      alignment: Alignment.centerLeft
                  ),
                )
            ),
          ],
        );
      }
    );
  }
}
